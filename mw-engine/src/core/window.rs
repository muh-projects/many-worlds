use crate::core::event::Event;

pub type EventCallback = Box<dyn Fn(&Event) -> bool>;

pub trait Window {
    fn on_update(&mut self, time: f64);
    fn width(&self) -> u32;
    fn height(&self) -> u32;
    fn size(&self) -> (u32, u32) {
        (self.width(), self.height())
    }
    fn set_event_callback(&mut self, ecb: EventCallback);
    fn set_vsync(&mut self, vsync: bool);
    fn is_vsync(&self) -> bool;
}

pub struct WindowParameters {
    pub width: u32,
    pub height: u32,
    pub title: &'static str,
}
impl WindowParameters {
    pub fn new(width: u32, height: u32, title: &'static str) -> Self {
        WindowParameters {
            width,
            height,
            title,
        }
    }
}
