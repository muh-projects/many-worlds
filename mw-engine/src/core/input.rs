use crate::core::bit;
use crate::core::event::Event;
use std::collections::HashSet;
use std::fmt::{Debug, Formatter};
use std::ops::{BitAnd, BitOr};

/// An enumeration of keys supported as input by the engine.
#[derive(Debug, Clone, Copy)]
pub enum Key {
    Unknown,
    Space,
    Apostrophe,
    Comma,
    Minus,
    Plus,
    Period,
    Slash,
    Num0,
    Num1,
    Num2,
    Num3,
    Num4,
    Num5,
    Num6,
    Num7,
    Num8,
    Num9,
    Semicolon,
    Equal,
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
    LeftBracket,
    Backslash,
    RightBracket,
    GraveAccent, /* ` */
    Escape,
    Enter,
    Tab,
    Backspace,
    Insert,
    Delete,
    Right,
    Left,
    Down,
    Up,
    PageUp,
    PageDown,
    Home,
    End,
    CapsLock,
    ScrollLock,
    NumLock,
    PrintScreen,
    Pause,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
    F13,
    F14,
    F15,
    F16,
    F17,
    F18,
    F19,
    F20,
    F21,
    F22,
    F23,
    F24,
    F25,
    Kp0,
    Kp1,
    Kp2,
    Kp3,
    Kp4,
    Kp5,
    Kp6,
    Kp7,
    Kp8,
    Kp9,
    KpDecimal,
    KpDivide,
    KpMultiply,
    KpSubtract,
    KpAdd,
    KpEnter,
    KpEqual,
    LeftShift,
    LeftControl,
    LeftAlt,
    LeftMeta,
    RightShift,
    RightControl,
    RightAlt,
    RightMeta,
    Menu,
}

impl Key {
    /// Whether or not the key is commonly used as a modifier (like Shift, Control, ...). See
    /// [Modifier] for a complete list.
    pub fn is_modifier(&self) -> bool {
        use Key::*;
        match self {
            LeftControl | LeftAlt | LeftShift | LeftMeta | RightMeta | RightControl | RightAlt
            | RightShift => true,
            _ => false,
        }
    }
}

/// An enumeration of mouse buttons supported as input by the engine.
#[derive(Debug, Clone, Copy)]
pub enum MouseButton {
    Left,
    Middle,
    Right,
}

/// An enumeration of keys interpreted as modifier by the engine.
#[derive(Debug, Clone, Copy)]
pub enum Modifier {
    None,
    LeftControl,
    LeftAlt,
    LeftShift,
    LefMeta,
    RightMeta,
    RightControl,
    RightAlt,
    RightShift,
}
impl Modifier {
    fn to_bitfield(&self) -> u16 {
        use Modifier::*;
        match self {
            None => 0,
            LeftControl => bit!(1),
            LeftAlt => bit!(2),
            LeftShift => bit!(3),
            LefMeta => bit!(4),
            RightMeta => bit!(5),
            RightControl => bit!(6),
            RightAlt => bit!(7),
            RightShift => bit!(8),
        }
    }
}

/// A bitmask of pressed modifier keys
#[derive(Clone, Copy)]
pub struct Modifiers(u16);

impl Debug for Modifiers {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:016b}", self.0)
    }
}

impl Modifiers {
    /// Any Shift key. Used for comparison (e.g. `some_modifier & Modifiers::shift()`)
    pub fn shift() -> Modifiers {
        Modifier::LeftShift | Modifier::RightShift
    }

    /// Any Control key. Used for comparison (e.g. `some_modifier & Modifiers::control()`)
    pub fn control() -> Modifiers {
        Modifier::LeftControl | Modifier::RightControl
    }

    /// Any Alt key. Used for comparison (e.g. `some_modifier & Modifiers::alt()`)
    pub fn alt() -> Modifiers {
        Modifier::LeftAlt | Modifier::RightAlt
    }

    /// Any Meta key. Used for comparison (e.g. `some_modifier & Modifiers::meta()`)
    pub fn meta() -> Modifiers {
        Modifier::LefMeta | Modifier::RightMeta
    }

    /// No modifiers
    pub fn none() -> Modifiers {
        Modifiers(Modifier::None.to_bitfield())
    }

    /// Creates a bit mask of modifiers with all bits set except the one corresponding to the
    /// supplied modifier.
    pub fn except(modifier: Modifier) -> Modifiers {
        Modifiers((!Modifier::None.to_bitfield()) ^ modifier.to_bitfield())
    }

    /// Whether this set of modifiers includes any Shift key.
    pub fn is_shift(&self) -> bool {
        self.0 & Modifiers::shift().0 > 0
    }

    /// Whether this set of modifiers includes any Alt key.
    pub fn is_alt(&self) -> bool {
        self.0 & Modifiers::alt().0 > 0
    }

    /// Whether this set of modifiers includes any Control key.
    pub fn is_control(&self) -> bool {
        self.0 & Modifiers::control().0 > 0
    }

    /// Whether this set of modifiers includes any Meta key.
    pub fn is_meta(&self) -> bool {
        self.0 & Modifiers::meta().0 > 0
    }
}

impl BitOr for Modifier {
    type Output = Modifiers;

    fn bitor(self, rhs: Self) -> Self::Output {
        Modifiers(self.to_bitfield() | rhs.to_bitfield())
    }
}

impl BitOr<Modifier> for Modifiers {
    type Output = Modifiers;

    fn bitor(self, rhs: Modifier) -> Self::Output {
        Modifiers(self.0 | rhs.to_bitfield())
    }
}

impl BitAnd<Modifiers> for Modifiers {
    type Output = Modifiers;

    fn bitand(self, rhs: Modifiers) -> Self::Output {
        Modifiers(self.0 & rhs.0)
    }
}

/// An accumulation of sub-states related to input, like the set of pressed keys and the current
/// cursor position. A global instance can be retrieved like so:
/// ```no_run
/// use mw_engine::core::Engine;
/// let input_state = Engine::input();
/// ```
pub struct InputState {
    #[allow(unused)]
    pressed_keys: HashSet<Key>, // TODO maybe use bitfield
    active_modifiers: Modifiers,
    cursor_position: (u32, u32),
    #[allow(unused)]
    pressed_mouse_buttons: HashSet<MouseButton>, // TODO maybe use bitfield
}
impl InputState {
    /// Creates an empty state with no keys pressed, no active modifiers, no mouse buttons pressed,
    /// and the window courser resting at coordinates (0,0).
    pub fn new() -> Self {
        InputState {
            pressed_keys: HashSet::new(),
            active_modifiers: Modifiers::none(),
            cursor_position: (0, 0),
            pressed_mouse_buttons: HashSet::new(),
        }
    }

    pub fn cursor_position(&self) -> (u32, u32) {
        self.cursor_position
    }

    pub fn set_cursor_position(&mut self, position: (u32, u32)) {
        self.cursor_position = position;
    }

    pub fn active_modifiers(&self) -> Modifiers {
        self.active_modifiers
    }

    pub fn add_active_modifier(&mut self, modifier: Modifier) {
        self.active_modifiers = self.active_modifiers | modifier
    }

    pub fn remove_active_modifier(&mut self, modifier: Modifier) {
        self.active_modifiers = self.active_modifiers & Modifiers::except(modifier)
    }

    /// Updates the input state according to this event. For example: If the supplied event
    /// is a CursorMoved event, the state will be updated to host the new cursor position.
    pub fn update(&mut self, event: &Event) {
        match event {
            // If the mouse has moved, just update the cursor position
            Event::CursorMoved(info) => self.set_cursor_position((info.x, info.y)),

            // If a key was pressed, this could either be a modifier or a regular key press
            Event::KeyPressed(info) => {
                match info.key {
                    // if the pressed key is a modifier: add this to the set of active modifiers
                    Key::RightAlt => self.add_active_modifier(Modifier::RightAlt),
                    Key::LeftAlt => self.add_active_modifier(Modifier::LeftAlt),
                    Key::RightControl => self.add_active_modifier(Modifier::RightControl),
                    Key::LeftControl => self.add_active_modifier(Modifier::LeftControl),
                    Key::LeftShift => self.add_active_modifier(Modifier::LeftShift),
                    Key::RightShift => self.add_active_modifier(Modifier::RightShift),
                    Key::RightMeta => self.add_active_modifier(Modifier::RightMeta),
                    Key::LeftMeta => self.add_active_modifier(Modifier::RightMeta),
                    _ => (),
                }
            }
            // If a key was released, this could either be a modifier or a regular key release
            Event::KeyReleased(info) => {
                match info.key {
                    // if the pressed key is a modifier: add this to the set of active modifiers
                    Key::RightAlt => self.remove_active_modifier(Modifier::RightAlt),
                    Key::LeftAlt => self.remove_active_modifier(Modifier::LeftAlt),
                    Key::RightControl => self.remove_active_modifier(Modifier::RightControl),
                    Key::LeftControl => self.remove_active_modifier(Modifier::LeftControl),
                    Key::LeftShift => self.remove_active_modifier(Modifier::LeftShift),
                    Key::RightShift => self.remove_active_modifier(Modifier::RightShift),
                    Key::RightMeta => self.remove_active_modifier(Modifier::RightMeta),
                    Key::LeftMeta => self.remove_active_modifier(Modifier::RightMeta),
                    _ => (),
                }
            }
            _ => (),
        }
    }
}
