use crate::core::event::Event;
use crate::core::layer::LayerStack;

/// Abstracts event handling, updating, and rendering across a layer stack. A scene is what an
/// [Application][crate::core::application::Application] mounts, and also what a viewport in the engine editor
/// displays.
pub struct Scene {
    layer_stack: LayerStack,
}

impl Scene {
    /// Creates an empty scene
    pub fn new() -> Self {
        Scene {
            layer_stack: LayerStack::new(),
        }
    }

    pub fn layer_stack(&mut self) -> &mut LayerStack {
        &mut self.layer_stack
    }

    pub fn on_render(&mut self) {
        for layer in self.layer_stack().top_down_iter_mut().rev() {
            if layer.is_enabled() {
                layer.on_render();
            }
        }
    }

    pub fn on_update(&mut self) {
        for layer in self.layer_stack().top_down_iter_mut().rev() {
            if layer.is_enabled() {
                layer.on_update();
            }
        }
    }

    pub fn on_event(&mut self, event: &Event) -> bool {
        let mut handled = false;
        let mut it = self.layer_stack().top_down_iter_mut();
        while let (Some(layer), false) = (it.next(), handled) {
            if layer.is_enabled() {
                handled = layer.on_event(event);
            }
        }
        handled
    }
}
