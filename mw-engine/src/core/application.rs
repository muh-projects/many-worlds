use std::sync::mpsc::{self, Receiver};

use crate::core::event::{Event, WindowResizedInfo};
use crate::core::scene::Scene;
use crate::core::window::{Window, WindowParameters};
use crate::core::Engine;
use log::trace;

/// The main, frontend entrypoint to application created with the engine. For more instructions,
/// see [Application::new].
pub struct Application {
    /// Whether the main loop should continue to iterate.
    running: bool,

    /// The (currently only) window associated with this engine instance.
    window: Box<dyn Window>,

    /// The currently displayed scene.
    scene: Scene,

    /// Timestamp of the last entry to the game loop
    last_update: f64,

    event_receiver: Receiver<Event>,
}

impl Application {
    /// Creates a new (single-window) application.
    ///
    /// In order to create an engine application, one needs to instantiate an application and
    /// set a scene:
    ///
    /// ```no_run
    /// use mw_engine::core::application::Application;
    /// use mw_engine::core::scene::Scene;
    ///
    /// let mut app = Application::new();
    /// // an empty scene, in this case
    /// app.set_scene(Scene::new());
    /// app.run();
    /// ```
    /// See [Application::run]
    pub fn new() -> Self {
        Engine::initialize();

        let (sender, receiver) = mpsc::channel();
        let mut app = Application {
            running: true,
            window: Engine::platform().new_window(&WindowParameters::new(
                1000,
                700,
                "Hello Many Worlds",
            )),
            scene: Scene::new(),
            last_update: 0.0,
            event_receiver: receiver,
        };

        // send window events to application instance
        // let app_ptr = &mut app as *mut Application;
        // unsafe {
        // app.window
        // .set_event_callback(Box::new(move |e| (*app_ptr).on_event(e)));
        // }

        // ^^^^^^^^^^^^ Idk why in the world I thought that's a good idea.
        // `app` gets moved out at the end of `new()`, meaning that the pointer
        // becomes dangling and any subsequent event callback results in invalid
        // memory access. I took some inspiration from the way GLFW handles it:
        app.window
            .set_event_callback(Box::new(move |e| sender.send(e.clone()).is_ok()));
        // But I might as well ask someone for the rusty way to do this.

        app
    }

    /// Executes the main game loop.
    pub fn run(&mut self) {
        while self.running {
            let time = Engine::platform().time();
            if time - self.last_update > 1.0 / 60.0 {
                self.update(time);
                self.last_update = time;
            }
            self.render(time);
        }
    }

    /// Sets a new scene to be displayed by the application window.
    pub fn set_scene(&mut self, scene: Scene) {
        self.scene = scene;
    }

    /// Handles events emitted by the windowing and input system.
    fn on_event(&mut self, event: &Event) -> bool {
        trace!("Application received event: {:?}", event);
        Engine::input().update(event);
        let handled = match event {
            Event::WindowClosed => self.on_window_close(),
            Event::WindowResized(info) => self.on_window_resize(info),
            _ => false,
        };
        if !handled {
            self.scene.on_event(event)
        } else {
            true
        }
    }

    /// A function that is called 60 times per second and updates the application state.
    fn update(&mut self, time: f64) {
        self.window.on_update(time);
        while let Ok(e) = self.event_receiver.try_recv() {
            self.on_event(&e);
        }
        self.scene.on_update();
    }

    /// Guess what this does (:
    fn render(&mut self, _time: f64) {
        self.scene.on_render();
    }

    /// Sops the entire application, shutting down the engine and exiting the process.
    fn stop(&mut self) {
        self.running = false;
    }

    /// The event handler for [WindowClosed][crate::core::event::Event::WindowClosed] events.
    fn on_window_close(&mut self) -> bool {
        self.stop();
        true
    }

    /// The event handler for [WindowResized][crate::core::event::Event::WindowResized] events.
    fn on_window_resize(&mut self, _info: &WindowResizedInfo) -> bool {
        false
    }
}
