//! The core module of the engine provides abstractions for common subsystems such as the event
//! system and windowing. The graphics-library-agnostic API for the renderer can be found in the
//! [renderer] module.

pub mod application;
pub mod event;
pub mod input;
pub mod layer;
pub mod scene;
pub mod window;

macro_rules! bit {
    ( $i:expr ) => {{
        1 << $i
    }};
}

pub(crate) use bit;
use fern::colors::{Color, ColoredLevelConfig};
use log::info;

use crate::core::input::InputState;
use crate::core::window::{Window, WindowParameters};
use crate::platform::initialize_platform;

/// The single engine instance (global mutable state, as stinky as it gets) that owns all the
/// engine's subsystem instances.
static mut ENGINE: Option<Engine> = None;

/// A small internal utility for abstractly querying the engine's initialization state.
fn is_engine_initialized() -> bool {
    unsafe { ENGINE.is_some() }
}

/// A struct that represents the engine state.
pub struct Engine {
    /// Global IO (mouse, keyboard, joystick, ...)
    input_state: InputState,
    /// The implementation of the platform abstraction loaded for this runtime.
    platform: Box<dyn Platform>,
}
impl Engine {
    /// Provides access to global IO like the currently active modifier keys, the cursor position, etc.
    pub fn input() -> &'static mut InputState {
        &mut Engine::instance().input_state
    }

    /// Provides access to the platform abstraction for creating windows, querying the system
    /// timer, etc.
    pub fn platform() -> &'static mut Box<dyn Platform> {
        &mut Engine::instance().platform
    }

    /// The main, backend entrypoint for the engine, panics if called more than once.
    pub fn initialize() {
        unsafe {
            if is_engine_initialized() {
                panic!("Engine has been initialized multiple times.");
            }
            // Logging needs to be initialized first since subsequent stages use logs already.
            initialize_logger();
            // Platform is next since it is the foundation of all systems building on top of it.
            let platform = initialize_platform();
            // InputState doesn't have any dependencies, it's a simple struct.
            let input_state = InputState::new();
            ENGINE = Some(Engine {
                input_state,
                platform,
            });
            info!("Initialized: Engine");
        }
    }

    /// Provides access to the engine singleton.
    fn instance() -> &'static mut Self {
        unsafe {
            if !is_engine_initialized() {
                panic!("Engine has not been initialized.")
            }
            ENGINE.as_mut().unwrap()
        }
    }
}

/// The core trait for platform abstraction. Implementations are located in the submodules of the [platform][crate::platform] module.
/// See:
/// - [linux] (Linux & GLFW)
/// - [opengl]
///
/// The operating system and windowing API is fixed at compile time, whereas the graphics library to
/// be used is chosen at runtime (though not all implementations will be available on all platforms).
pub trait Platform {
    /// Creates a new [Window].
    fn new_window(&mut self, params: &WindowParameters) -> Box<dyn Window>;
    /// Returns the time in seconds since the application has been launched.
    fn time(&self) -> f64;
}

fn initialize_logger() {
    let mut colors = ColoredLevelConfig::new();
    colors.warn = Color::Yellow;
    colors.error = Color::Red;
    colors.debug = Color::BrightBlack;
    colors.trace = Color::Magenta;
    colors.info = Color::BrightWhite;

    fern::Dispatch::new()
        .format(move |out, message, record| {
            out.finish(format_args!(
                "{}[{}][{}] {}",
                // chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                chrono::Local::now().format("[%H:%M:%S]"),
                record.target(),
                colors.color(record.level()),
                message
            ))
        })
        .level(log::LevelFilter::Debug)
        .chain(std::io::stdout())
        .apply()
        .expect("An error occurred while setting up the logger.");
}
