use crate::core::input::{Key, MouseButton};
use std::fmt::Debug;
use std::ops::BitOr;

use crate::core::bit;

#[derive(Debug, Clone, Copy)]
pub struct WindowResizedInfo {
    /// The new width of the window.
    pub width: i32,

    /// The new height of the window.
    pub height: i32,
}

#[derive(Debug, Clone, Copy)]
pub struct WindowMovedInfo {
    /// The new horizontal position of the window.
    pub x: i32,
    /// The new vertical position of the window.
    pub y: i32,
}

#[derive(Debug, Clone, Copy)]
pub struct KeyPressedInfo {
    pub key: Key,
}

#[derive(Debug, Clone, Copy)]
pub struct KeyReleasedInfo {
    pub key: Key,
}

#[derive(Debug, Clone, Copy)]
pub struct MouseButtonPressedInfo {
    pub mouse_button: MouseButton,
}

#[derive(Debug, Clone, Copy)]
pub struct MouseButtonReleasedInfo {
    pub mouse_button: MouseButton,
}

#[derive(Debug, Clone, Copy)]
pub struct CursorMovedInfo {
    /// The new X-coordinate of the cursor.
    pub x: u32,
    /// The new Y-coordinate of the cursor.
    pub y: u32,
}

#[derive(Debug, Clone, Copy)]
pub struct MouseScrolledInfo {
    /// The sign of this value indicates the direction the scroll.
    pub distance: i32,
}

///  _The_ main event concept of the engine. It is platform-/framework-independent and the mapping
/// from any particular framework to the agnostic API is found in the respective implementation in
/// `/platform/<platform>.rs`.
#[derive(Debug, Clone, Copy)]
pub enum Event {
    None,
    // window events
    WindowClosed,
    WindowResized(WindowResizedInfo),
    WindowFocused,
    WindowLostFocus,
    WindowMoved(WindowMovedInfo),
    // app events
    AppTick,
    AppUpdate,
    AppRender,
    // key events
    KeyPressed(KeyPressedInfo),
    KeyReleased(KeyReleasedInfo),
    // mouse events
    MouseButtonPressed(MouseButtonPressedInfo),
    MouseButtonReleased(MouseButtonReleasedInfo),
    CursorMoved(CursorMovedInfo),
    MouseScrolled(MouseScrolledInfo),
}

impl Event {
    /// The set of categories this event belongs to.
    pub fn categories(&self) -> EventCategoryMask {
        use EventCategory::*;
        match self {
            Event::None => None | None,

            Event::WindowClosed
            | Event::WindowResized(_)
            | Event::WindowFocused
            | Event::WindowLostFocus
            | Event::WindowMoved(_) => Application | Window,

            Event::AppTick | Event::AppUpdate | Event::AppRender => Application | None,

            Event::KeyPressed(_) | Event::KeyReleased(_) => Input | Keyboard,

            Event::MouseButtonPressed(_)
            | Event::MouseButtonReleased(_)
            | Event::CursorMoved(_)
            | Event::MouseScrolled(_) => Input | Mouse,
        }
    }
}

/// All event types fall into one or multiple categories so that engine components can filter
/// events based on their category.
pub enum EventCategory {
    None = 0,
    Application,
    Window,
    Input,
    Keyboard,
    Mouse,
}

impl EventCategory {
    /// Returns the bit mask that represents the event category of `self`.
    fn to_bitfield(&self) -> u16 {
        use EventCategory::*;
        match self {
            None => 0,
            Application => bit!(0),
            Window => bit!(1),
            Input => bit!(2),
            Keyboard => bit!(3),
            Mouse => bit!(4),
        }
    }
}

pub type EventCategoryMask = u16;

impl BitOr for EventCategory {
    type Output = EventCategoryMask;
    fn bitor(self, rhs: Self) -> Self::Output {
        self.to_bitfield() | rhs.to_bitfield()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn debug_event() {
        let event = Event::KeyPressed(KeyPressedInfo { key: Key::W });
        assert_eq!(
            "KeyPressed(KeyPressedInfo { key: W })",
            format!("{:?}", event)
        );
    }
}
