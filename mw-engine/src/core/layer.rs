use crate::core::event::Event;

pub trait Layer {
    fn name(&self) -> &str;
    fn on_update(&mut self);
    fn on_render(&mut self);
    fn on_event(&mut self, event: &Event) -> bool;
    fn is_enabled(&self) -> bool;
    fn enable(&mut self);
    fn disable(&mut self);
}

pub struct LayerStack {
    layers: Vec<Box<dyn Layer>>,
    overlays: Vec<Box<dyn Layer>>,
}
impl LayerStack {
    pub fn new() -> Self {
        LayerStack {
            layers: Vec::new(),
            overlays: Vec::new(),
        }
    }

    pub fn push_layer(&mut self, layer: Box<dyn Layer>) {
        self.layers.push(layer);
    }

    pub fn push_overlay(&mut self, layer: Box<dyn Layer>) {
        self.overlays.push(layer);
    }

    pub fn pop_layer(&mut self) -> Option<Box<dyn Layer>> {
        self.layers.pop()
    }

    pub fn pop_overlay(&mut self) -> Option<Box<dyn Layer>> {
        self.overlays.pop()
    }

    pub fn top_down_iter_mut(&mut self) -> impl DoubleEndedIterator<Item = &mut Box<dyn Layer>> {
        self.layers.iter_mut().chain(self.overlays.iter_mut()).rev()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::core::event::Event;
    use log::debug;

    struct TestLayer {
        name: String,
        enabled: bool,
    }

    impl Layer for TestLayer {
        fn name(&self) -> &str {
            &self.name
        }

        fn on_update(&mut self) {
            debug!("Updating {}", self.name());
        }

        fn on_render(&mut self) {
            debug!("Rendering {}", self.name());
        }

        fn on_event(&mut self, event: &Event) -> bool {
            debug!("{} received {:?}", self.name(), event);
            false
        }

        fn is_enabled(&self) -> bool {
            self.enabled
        }

        fn enable(&mut self) {
            self.enabled = true;
        }

        fn disable(&mut self) {
            self.enabled = false;
        }
    }

    // TODO test on_attach and on_detach, as well as on_event in combination with `enabled`

    #[test]
    fn test_top_down_only_layers() {
        let mut stack = LayerStack::new();
        let l1 = Box::new(TestLayer {
            name: "L1".into(),
            enabled: false,
        });
        let l2 = Box::new(TestLayer {
            name: "L2".into(),
            enabled: true,
        });

        stack.push_layer(l1);
        stack.push_layer(l2);

        assert_eq!(stack.layers.len(), 2);
        assert_eq!(stack.overlays.len(), 0);

        let mut iter = stack.top_down_iter_mut();
        // TODO test equality -> find out how layers "equal"
        assert_eq!(iter.next().unwrap().name(), "L2");
        assert_eq!(iter.next().unwrap().name(), "L1");
        assert!(iter.next().is_none());
    }

    #[test]
    fn test_top_down_only_overlays() {
        let mut stack = LayerStack::new();
        let l1 = Box::new(TestLayer {
            name: "L1".into(),
            enabled: false,
        });
        let l2 = Box::new(TestLayer {
            name: "L2".into(),
            enabled: true,
        });

        stack.push_overlay(l1);
        stack.push_overlay(l2);

        assert_eq!(stack.layers.len(), 0);
        assert_eq!(stack.overlays.len(), 2);

        let mut iter = stack.top_down_iter_mut();
        assert_eq!(iter.next().unwrap().name(), "L2");
        assert_eq!(iter.next().unwrap().name(), "L1");
        assert!(iter.next().is_none());
    }

    #[test]
    fn test_top_down_both_vecs() {
        let mut stack = LayerStack::new();
        let l1 = Box::new(TestLayer {
            name: "L1".into(),
            enabled: false,
        });
        let l2 = Box::new(TestLayer {
            name: "L2".into(),
            enabled: true,
        });

        stack.push_layer(l1);
        stack.push_overlay(l2);

        assert_eq!(stack.layers.len(), 1);
        assert_eq!(stack.overlays.len(), 1);

        let mut iter = stack.top_down_iter_mut();
        assert_eq!(iter.next().unwrap().name(), "L2");
        assert_eq!(iter.next().unwrap().name(), "L1");
        assert!(iter.next().is_none());
    }

    #[test]
    fn test_top_down_empty() {
        let mut stack = LayerStack::new();
        assert_eq!(stack.layers.len(), 0);
        assert_eq!(stack.overlays.len(), 0);

        let mut iter = stack.top_down_iter_mut();
        assert!(iter.next().is_none());
    }

    #[test]
    fn test_bottom_up_only_layers() {
        let mut stack = LayerStack::new();
        let l1 = Box::new(TestLayer {
            name: "L1".into(),
            enabled: false,
        });
        let l2 = Box::new(TestLayer {
            name: "L2".into(),
            enabled: true,
        });

        stack.push_layer(l1);
        stack.push_layer(l2);

        assert_eq!(stack.layers.len(), 2);
        assert_eq!(stack.overlays.len(), 0);

        let mut iter = stack.top_down_iter_mut().rev();
        assert_eq!(iter.next().unwrap().name(), "L1");
        assert_eq!(iter.next().unwrap().name(), "L2");
        assert!(iter.next().is_none());
    }

    #[test]
    fn test_bottom_up_only_overlays() {
        let mut stack = LayerStack::new();
        let l1 = Box::new(TestLayer {
            name: "L1".into(),
            enabled: false,
        });
        let l2 = Box::new(TestLayer {
            name: "L2".into(),
            enabled: true,
        });

        stack.push_overlay(l1);
        stack.push_overlay(l2);

        assert_eq!(stack.layers.len(), 0);
        assert_eq!(stack.overlays.len(), 2);

        let mut iter = stack.top_down_iter_mut().rev();
        assert_eq!(iter.next().unwrap().name(), "L1");
        assert_eq!(iter.next().unwrap().name(), "L2");
        assert!(iter.next().is_none());
    }

    #[test]
    fn test_bottom_up_both_vecs() {
        let mut stack = LayerStack::new();
        let l1 = Box::new(TestLayer {
            name: "L1".into(),
            enabled: false,
        });
        let l2 = Box::new(TestLayer {
            name: "L2".into(),
            enabled: true,
        });

        stack.push_layer(l1);
        stack.push_overlay(l2);

        assert_eq!(stack.layers.len(), 1);
        assert_eq!(stack.overlays.len(), 1);

        let mut iter = stack.top_down_iter_mut().rev();
        assert_eq!(iter.next().unwrap().name(), "L1");
        assert_eq!(iter.next().unwrap().name(), "L2");
        assert!(iter.next().is_none());
    }

    #[test]
    fn test_bottom_up_empty() {
        let mut stack = LayerStack::new();
        assert_eq!(stack.layers.len(), 0);
        assert_eq!(stack.overlays.len(), 0);

        let mut iter = stack.top_down_iter_mut().rev();
        assert!(iter.next().is_none());
    }
}
