use crate::core::event::{
    CursorMovedInfo, Event, KeyPressedInfo, KeyReleasedInfo, MouseButtonPressedInfo,
    MouseButtonReleasedInfo,
};
use crate::core::input::{Key, MouseButton};
use crate::core::window::{EventCallback, Window, WindowParameters};
use crate::core::Platform;
use glfw::{Callback, Context, Glfw, SwapInterval};
use log::{debug, error, info};
use std::ffi::CStr;
use std::sync::mpsc::Receiver;

/// A struct with references (not in the rusty sense) to all information relevant to the platform.
pub struct Linux {
    /// The global GLFW instance, can be assumed to be initialized.
    glfw: Glfw,
}

/// Performs all the relevant initialization for the Linux platform and its abstraction.
pub fn initialize_platform() -> Box<dyn Platform> {
    let boxx = Box::new(Linux {
        glfw: glfw::init(glfw::FAIL_ON_ERRORS).unwrap(),
    });
    info!("Initialized: Platform (Linux, GLFW)");
    boxx
}

impl Platform for Linux {
    fn new_window(&mut self, params: &WindowParameters) -> Box<dyn Window> {
        // create the glfw window pointer and an event receiver
        let (mut window, events) = self
            .glfw
            .create_window(
                params.width,
                params.height,
                params.title,
                glfw::WindowMode::Windowed,
            )
            .expect("Failed to create GLFW window.");
        window.make_current();
        window.set_all_polling(true);
        self.glfw.set_error_callback(Some(Callback {
            f: |e, s, _d| {
                error!("GLFW Error: {} {}", e, s);
            },
            data: (),
        }));

        // load OpenGL bindings (TODO abstract to Renderer::init())
        gl::load_with(|s| window.get_proc_address(s) as *const _);
        unsafe {
            let version = CStr::from_ptr(gl::GetString(gl::VERSION) as *mut i8)
                .to_str()
                .unwrap();
            let vendor = CStr::from_ptr(gl::GetString(gl::VENDOR) as *mut i8)
                .to_str()
                .unwrap();
            if version.is_empty() || vendor.is_empty() {
                panic!("Couldn't load OpenGL version or vendor!")
            } else {
                debug!("Loaded OpenGL:");
                debug!("\tVersion: {}", version);
                debug!("\tVendor: {}", vendor);
            }
        }

        let mut glfw_window = GlfwWindow::new(window, events);
        glfw_window.set_vsync(true);
        Box::new(glfw_window)
    }

    fn time(&self) -> f64 {
        self.glfw.get_time()
    }
}

struct GlfwWindow {
    /// The GLFW window object
    implementation: glfw::Window,
    /// A receiver used to poll events in GLFW
    events: Receiver<(f64, glfw::WindowEvent)>,
    /// Whether the window should refresh with the monitor's refresh rate.
    vsync: bool,
    /// A delegate for events emitted by GLFW.
    callback: Option<EventCallback>,
}

impl GlfwWindow {
    fn new(implementation: glfw::Window, events: Receiver<(f64, glfw::WindowEvent)>) -> Self {
        GlfwWindow {
            implementation,
            events,
            vsync: false,
            callback: None,
        }
    }

    /// Polls the GLFW event queue, maps GLFW events and information to the engine's framework-agnostic
    /// event API and invokes the event callback set on the this instance.
    fn dispatch_events(&mut self) {
        for (_, event) in glfw::flush_messages(&self.events) {
            // debug!("GlfwWindow event: {:?}", event);
            let mut wrapped_event: Event = match event {
                glfw::WindowEvent::Key(key, _, action, _) => map_key_event(key, action),
                glfw::WindowEvent::MouseButton(button, action, _) => {
                    map_mouse_button_event(button, action)
                }
                glfw::WindowEvent::CursorPos(x, y) => map_cursor_move_event(x, y),
                glfw::WindowEvent::Close => map_close_event(),
                _ => Event::None,
            };

            if let Some(f) = &mut self.callback {
                f(&mut wrapped_event);
            }
        }
    }
}

/// Maps [glfw::WindowEvent::Close] to the corresponding mw_engine event type.
fn map_close_event() -> Event {
    Event::WindowClosed
}

/// Maps [glfw::WindowEvent::CursorPos] to the corresponding mw_engine event type.
fn map_cursor_move_event(x: f64, y: f64) -> Event {
    Event::CursorMoved(CursorMovedInfo {
        x: x as u32,
        y: y as u32,
    })
}

/// Maps [glfw::WindowEvent::MouseButton] to the corresponding mw_engine event type.
fn map_mouse_button_event(
    glfw_mouse_button: glfw::MouseButton,
    glfw_action: glfw::Action,
) -> Event {
    let mouse_button = map_mouse_button(glfw_mouse_button);
    match glfw_action {
        glfw::Action::Press => Event::MouseButtonPressed(MouseButtonPressedInfo { mouse_button }),
        glfw::Action::Release => {
            Event::MouseButtonReleased(MouseButtonReleasedInfo { mouse_button })
        }
        glfw::Action::Repeat => Event::None,
    }
}

/// Maps [glfw::WindowEvent::Key] to the corresponding mw_engine event type.
fn map_key_event(glfw_key: glfw::Key, glfw_action: glfw::Action) -> Event {
    let key = map_key(glfw_key);
    match glfw_action {
        glfw::Action::Press => Event::KeyPressed(KeyPressedInfo { key }),
        glfw::Action::Release => Event::KeyReleased(KeyReleasedInfo { key }),
        glfw::Action::Repeat => Event::None,
    }
}

/// Maps [glfw::Key] to the corresponding [engine Key][crate::core::input::Key].
fn map_key(glfw_key: glfw::Key) -> Key {
    match glfw_key {
        glfw::Key::LeftControl => Key::LeftControl,
        glfw::Key::RightControl => Key::RightControl,
        glfw::Key::LeftAlt => Key::LeftAlt,
        glfw::Key::RightAlt => Key::RightAlt,
        glfw::Key::LeftSuper => Key::LeftMeta,
        glfw::Key::RightSuper => Key::RightMeta,
        glfw::Key::LeftShift => Key::LeftShift,
        glfw::Key::RightShift => Key::RightShift,
        glfw::Key::Space => Key::Space,
        glfw::Key::Apostrophe => Key::Apostrophe,
        glfw::Key::Comma => Key::Comma,
        glfw::Key::Minus => Key::Minus,
        glfw::Key::Period => Key::Period,
        glfw::Key::Slash => Key::Slash,
        glfw::Key::Num0 => Key::Num0,
        glfw::Key::Num1 => Key::Num1,
        glfw::Key::Num2 => Key::Num2,
        glfw::Key::Num3 => Key::Num3,
        glfw::Key::Num4 => Key::Num4,
        glfw::Key::Num5 => Key::Num5,
        glfw::Key::Num6 => Key::Num6,
        glfw::Key::Num7 => Key::Num7,
        glfw::Key::Num8 => Key::Num8,
        glfw::Key::Num9 => Key::Num9,
        glfw::Key::Semicolon => Key::Semicolon,
        glfw::Key::Equal => Key::Equal,
        glfw::Key::A => Key::A,
        glfw::Key::B => Key::B,
        glfw::Key::C => Key::C,
        glfw::Key::D => Key::D,
        glfw::Key::E => Key::E,
        glfw::Key::F => Key::F,
        glfw::Key::G => Key::G,
        glfw::Key::H => Key::H,
        glfw::Key::I => Key::I,
        glfw::Key::J => Key::J,
        glfw::Key::K => Key::K,
        glfw::Key::L => Key::L,
        glfw::Key::M => Key::M,
        glfw::Key::N => Key::N,
        glfw::Key::O => Key::O,
        glfw::Key::P => Key::P,
        glfw::Key::Q => Key::Q,
        glfw::Key::R => Key::R,
        glfw::Key::S => Key::S,
        glfw::Key::T => Key::T,
        glfw::Key::U => Key::U,
        glfw::Key::V => Key::V,
        glfw::Key::W => Key::W,
        glfw::Key::X => Key::X,
        glfw::Key::Y => Key::Y,
        glfw::Key::Z => Key::Z,
        glfw::Key::LeftBracket => Key::LeftBracket,
        glfw::Key::Backslash => Key::Backslash,
        glfw::Key::RightBracket => Key::RightBracket,
        glfw::Key::GraveAccent => Key::GraveAccent,
        glfw::Key::Escape => Key::Escape,
        glfw::Key::Enter => Key::Enter,
        glfw::Key::Tab => Key::Tab,
        glfw::Key::Backspace => Key::Backspace,
        glfw::Key::Insert => Key::Insert,
        glfw::Key::Delete => Key::Delete,
        glfw::Key::Right => Key::Right,
        glfw::Key::Left => Key::Left,
        glfw::Key::Down => Key::Down,
        glfw::Key::Up => Key::Up,
        glfw::Key::PageUp => Key::PageUp,
        glfw::Key::PageDown => Key::PageDown,
        glfw::Key::Home => Key::Home,
        glfw::Key::End => Key::End,
        glfw::Key::CapsLock => Key::CapsLock,
        glfw::Key::ScrollLock => Key::ScrollLock,
        glfw::Key::NumLock => Key::NumLock,
        glfw::Key::PrintScreen => Key::PrintScreen,
        glfw::Key::Pause => Key::Pause,
        glfw::Key::F1 => Key::F1,
        glfw::Key::F2 => Key::F2,
        glfw::Key::F3 => Key::F3,
        glfw::Key::F4 => Key::F4,
        glfw::Key::F5 => Key::F5,
        glfw::Key::F6 => Key::F6,
        glfw::Key::F7 => Key::F7,
        glfw::Key::F8 => Key::F8,
        glfw::Key::F9 => Key::F9,
        glfw::Key::F10 => Key::F10,
        glfw::Key::F11 => Key::F11,
        glfw::Key::F12 => Key::F12,
        glfw::Key::F13 => Key::F13,
        glfw::Key::F14 => Key::F14,
        glfw::Key::F15 => Key::F15,
        glfw::Key::F16 => Key::F16,
        glfw::Key::F17 => Key::F17,
        glfw::Key::F18 => Key::F18,
        glfw::Key::F19 => Key::F19,
        glfw::Key::F20 => Key::F20,
        glfw::Key::F21 => Key::F21,
        glfw::Key::F22 => Key::F22,
        glfw::Key::F23 => Key::F23,
        glfw::Key::F24 => Key::F24,
        glfw::Key::F25 => Key::F25,
        glfw::Key::Kp0 => Key::Kp0,
        glfw::Key::Kp1 => Key::Kp1,
        glfw::Key::Kp2 => Key::Kp2,
        glfw::Key::Kp3 => Key::Kp3,
        glfw::Key::Kp4 => Key::Kp4,
        glfw::Key::Kp5 => Key::Kp5,
        glfw::Key::Kp6 => Key::Kp6,
        glfw::Key::Kp7 => Key::Kp7,
        glfw::Key::Kp8 => Key::Kp8,
        glfw::Key::Kp9 => Key::Kp9,
        glfw::Key::KpDecimal => Key::KpDecimal,
        glfw::Key::KpDivide => Key::KpDivide,
        glfw::Key::KpMultiply => Key::KpMultiply,
        glfw::Key::KpSubtract => Key::KpSubtract,
        glfw::Key::KpAdd => Key::KpAdd,
        glfw::Key::KpEnter => Key::KpEnter,
        glfw::Key::KpEqual => Key::KpEqual,
        glfw::Key::Menu => Key::Menu,

        // unknown keys
        glfw::Key::Unknown | glfw::Key::World1 | glfw::Key::World2 => Key::Unknown,
    }
}

// fn map_modifiers(mods: glfw::Modifiers) -> Modifiers {
//     match mods {
//         glfw::Modifiers::Shift => Modifier::shift(),
//         glfw::Modifiers::Control => Modifier::control(),
//         glfw::Modifiers::Alt => Modifier::alt(),
//         glfw::Modifiers::Super => Modifier::meta(),
//         _ => Modifier::none(),
//     }
// }

/// Maps [glfw::MouseButton] to the corresponding [engine MouseButton][crate::core::input::MouseButton].
fn map_mouse_button(mb: glfw::MouseButton) -> MouseButton {
    match mb {
        glfw::MouseButton::Button1 => MouseButton::Left,
        glfw::MouseButton::Button2 => MouseButton::Right,
        glfw::MouseButton::Button3 => MouseButton::Middle,
        _ => unimplemented!(),
    }
}

impl Window for GlfwWindow {
    fn on_update(&mut self, _time: f64) {
        self.implementation.swap_buffers();
        self.implementation.glfw.poll_events();
        self.dispatch_events();
    }

    fn width(&self) -> u32 {
        self.implementation.get_size().0 as u32
    }

    fn height(&self) -> u32 {
        self.implementation.get_size().1 as u32
    }

    fn set_event_callback(&mut self, ecb: EventCallback) {
        self.callback = Some(ecb);
    }

    fn set_vsync(&mut self, vsync: bool) {
        let interval = if vsync {
            SwapInterval::Sync(1)
        } else {
            SwapInterval::None
        };
        self.implementation.glfw.set_swap_interval(interval);
        self.vsync = vsync;
    }

    fn is_vsync(&self) -> bool {
        self.vsync
    }
}
