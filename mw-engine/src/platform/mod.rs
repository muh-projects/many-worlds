//! The `platform` module contains implementations of [Window][core::window::Window], [Platform][core::Platform], and [Renderer][renderer::Renderer]

#[cfg(target_os = "linux")]
pub use linux::initialize_platform;

#[cfg(target_os = "linux")]
mod linux;

#[cfg(any(target_os = "linux", target_os = "windows"))]
pub mod opengl;
