use crate::renderer::buffer::{Buffer, VertexArray};

pub struct OpenGLVertexArray {
    number: i32,
    id: u32,
}

impl OpenGLVertexArray {
    pub fn new(number: i32) -> Self {
        let mut id = 0;
        unsafe {
            gl::CreateVertexArrays(number, &mut id);
        }
        OpenGLVertexArray { number, id }
    }
}

impl Buffer for OpenGLVertexArray {
    fn bind(&mut self) {
        unsafe {
            gl::BindVertexArray(self.id);
        }
    }

    fn unbind(&mut self) {
        // nothing to do
    }
}

impl VertexArray for OpenGLVertexArray {}

impl Drop for OpenGLVertexArray {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteVertexArrays(self.number, &self.id);
        }
    }
}
