use crate::renderer::buffer::{Buffer, IndexBuffer};

pub struct OpenGLIndexBuffer {
    nr_of_indices: u32,
}

impl OpenGLIndexBuffer {
    pub fn new(nr_of_indices: u32) -> Self {
        OpenGLIndexBuffer { nr_of_indices }
    }
}

impl Buffer for OpenGLIndexBuffer {
    fn bind(&mut self) {
        todo!()
    }

    fn unbind(&mut self) {
        todo!()
    }
}

impl IndexBuffer for OpenGLIndexBuffer {
    fn nr_of_indices(&self) -> u32 {
        self.nr_of_indices
    }
}

impl Drop for OpenGLIndexBuffer {
    fn drop(&mut self) {
        todo!()
    }
}
