use crate::renderer::buffer::{Buffer, VertexBuffer};

pub struct OpenGLVertexBuffer {}

impl OpenGLVertexBuffer {
    pub fn new() -> Self {
        OpenGLVertexBuffer {}
    }
}

impl Buffer for OpenGLVertexBuffer {
    fn bind(&mut self) {
        todo!()
    }

    fn unbind(&mut self) {
        todo!()
    }
}

impl VertexBuffer for OpenGLVertexBuffer {}

impl Drop for OpenGLVertexBuffer {
    fn drop(&mut self) {
        todo!()
    }
}
