use crate::renderer::shader::{Shader, ShaderProgram, ShaderType};
use gl::types::*;
use std::ffi::CString;

pub struct OpenGLShader {
    id: GLuint,
}

impl OpenGLShader {
    pub fn new(source: String, shader_type: ShaderType) -> Self {
        unsafe {
            let id = gl::CreateShader(Self::api_type_to_gl(shader_type));
            let c_str = CString::new(source.as_bytes()).expect("Shader source couldn't be C-ified");
            gl::ShaderSource(id, 1, &c_str.as_ptr(), std::ptr::null());
            gl::CompileShader(id);
            let mut status = gl::FALSE as GLint;
            gl::GetShaderiv(id, gl::COMPILE_STATUS, &mut status);
            if status != (gl::TRUE as GLint) {
                Self::panic_with_gl_error(id);
            }
            OpenGLShader { id }
        }
    }

    fn api_type_to_gl(shader_type: ShaderType) -> GLuint {
        match shader_type {
            ShaderType::Vertex => gl::VERTEX_SHADER,
            ShaderType::Fragment => gl::FRAGMENT_SHADER,
            ShaderType::TesselationControl => gl::TESS_CONTROL_SHADER,
            ShaderType::TesselationEvaluation => gl::TESS_EVALUATION_SHADER,
            ShaderType::Compute => gl::COMPUTE_SHADER,
            ShaderType::Geometry => gl::GEOMETRY_SHADER,
        }
    }

    unsafe fn panic_with_gl_error(id: GLuint) {
        let mut len = 0;
        gl::GetShaderiv(id, gl::INFO_LOG_LENGTH, &mut len);
        let mut buf = Vec::with_capacity(len as usize);
        buf.set_len((len as usize) - 1); // subtract 1 to skip the trailing null character
        gl::GetShaderInfoLog(
            id,
            len,
            std::ptr::null_mut(),
            buf.as_mut_ptr() as *mut GLchar,
        );
        panic!(
            "{}",
            std::str::from_utf8(&buf)
                .ok()
                .expect("ShaderInfoLog not valid utf8")
        )
    }
}

impl Drop for OpenGLShader {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteShader(self.id);
        }
    }
}

impl Shader for OpenGLShader {
    fn id(&self) -> u32 {
        self.id
    }
}

pub struct OpenGLShaderProgram {
    id: GLuint,
}

impl OpenGLShaderProgram {
    pub fn new() -> Self {
        unsafe {
            OpenGLShaderProgram {
                id: gl::CreateProgram(),
            }
        }
    }
}

impl Drop for OpenGLShaderProgram {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteProgram(self.id);
        }
    }
}

impl ShaderProgram for OpenGLShaderProgram {
    fn id(&self) -> u32 {
        self.id
    }

    fn attach(&mut self, shader: Box<dyn Shader>) {
        unsafe { gl::AttachShader(self.id, shader.id()) }
    }

    fn link(&mut self) {
        unsafe {
            gl::LinkProgram(self.id);
        }
    }

    fn bind(&mut self) {
        unsafe {
            gl::UseProgram(self.id);
        }
    }
}
