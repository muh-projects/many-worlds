pub trait Shader {
    fn id(&self) -> u32;
}

pub enum ShaderType {
    Vertex,
    TesselationControl,
    TesselationEvaluation,
    Geometry,
    Compute,
    Fragment,
}

pub trait ShaderProgram {
    fn id(&self) -> u32;
    fn attach(&mut self, shader: Box<dyn Shader>);
    fn link(&mut self);
    fn bind(&mut self);
}
