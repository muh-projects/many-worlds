pub trait Buffer {
    fn bind(&mut self);
    fn unbind(&mut self);
}

pub trait VertexBuffer: Buffer {}

pub trait IndexBuffer: Buffer {
    fn nr_of_indices(&self) -> u32;
}

pub trait VertexArray: Buffer {}
