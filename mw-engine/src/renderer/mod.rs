use self::buffer::VertexArray;
use crate::platform::opengl::{
    OpenGLIndexBuffer, OpenGLShader, OpenGLShaderProgram, OpenGLVertexArray, OpenGLVertexBuffer,
};
use crate::renderer::buffer::{IndexBuffer, VertexBuffer};
use crate::renderer::shader::{Shader, ShaderProgram, ShaderType};

pub mod buffer;
pub mod shader;

pub enum GraphicsApi {
    OpenGL,
    DirectX11,
    Vulkan,
}

pub fn graphics_api() -> GraphicsApi {
    GraphicsApi::OpenGL
}

pub struct Shaders;

impl Shaders {
    pub fn new_shader(shader_type: ShaderType, source: String) -> Box<dyn Shader> {
        match graphics_api() {
            GraphicsApi::OpenGL => Box::new(OpenGLShader::new(source, shader_type)),
            GraphicsApi::DirectX11 => unimplemented!(),
            GraphicsApi::Vulkan => unimplemented!(),
        }
    }

    pub fn new_shader_program() -> Box<dyn ShaderProgram> {
        match graphics_api() {
            GraphicsApi::OpenGL => Box::new(OpenGLShaderProgram::new()),
            GraphicsApi::DirectX11 => unimplemented!(),
            GraphicsApi::Vulkan => unimplemented!(),
        }
    }
}

pub struct Buffers;

impl Buffers {
    pub fn new_vertex_buffer() -> Box<dyn VertexBuffer> {
        match graphics_api() {
            GraphicsApi::OpenGL => Box::new(OpenGLVertexBuffer::new()),
            GraphicsApi::DirectX11 => unimplemented!(),
            GraphicsApi::Vulkan => unimplemented!(),
        }
    }

    pub fn new_index_buffer(nr_of_indices: u32) -> Box<dyn IndexBuffer> {
        match graphics_api() {
            GraphicsApi::OpenGL => Box::new(OpenGLIndexBuffer::new(nr_of_indices)),
            GraphicsApi::DirectX11 => unimplemented!(),
            GraphicsApi::Vulkan => unimplemented!(),
        }
    }

    pub fn new_vertex_array(number: i32) -> Box<dyn VertexArray> {
        match graphics_api() {
            GraphicsApi::OpenGL => Box::new(OpenGLVertexArray::new(number)),
            GraphicsApi::DirectX11 => unimplemented!(),
            GraphicsApi::Vulkan => unimplemented!(),
        }
    }
}
