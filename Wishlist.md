Wishlist
===

- Particles (rain <3)
- 3d spatial audio
- World gen
- Seasons system
- Wavy foliage etc.
- Questing system & dungeons (world gen?)
- Vehicle support
- Path planning and AI stuff
- Networking (multiplayer)
- Scripting & modding with deno (?)

# Next up
- [x] layers
- [x] wrap all GLFW events in engine event types
- [x] use logging instead of println!()
- [x] Update build image (add Qt dependencies)
- [ ] Set up rendering for editor canvas (Qt)
- [ ] Add some `Time` struct for updates and rendering (incl. since startup and delta time)
- [ ] create bare minimum rendering primitives
  - [ ] vertex array
    - add_vertex_buffer
    - set_index_buffer
  - [ ] vertex buffer
  - [ ] index buffer
  - [ ] renderer api
    - set_viewport
    - init
    - clear
    - draw_indexed