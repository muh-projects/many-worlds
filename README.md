Many Worlds
===

> The playable game engine

A little hobby project of mine where I can fiddle around with rust and try out stuff I learn here and there.

## Structure
- `mw-engine`: Engine API and implementation (Lib)
- `mw-editor`: The application that allows modding and playing around with the engine (Bin)
- `mw-game`: The stand-alone game for distribution (Bin)