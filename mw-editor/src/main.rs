#![windows_subsystem = "windows"]

extern crate log;

use cpp_core::{Ptr, StaticUpcast};
use mw_engine::core::Engine;
use qt_core::{qs, slot, QBox, QObject, QSize, SlotNoArgs};
use qt_widgets::{QApplication, QHBoxLayout, QMessageBox, QOpenGLWidget, QPushButton, QWidget};
use std::rc::Rc;

struct Gui {
    widget: QBox<QWidget>,
    #[allow(unused)]
    canvas: QBox<QOpenGLWidget>,
    button: QBox<QPushButton>,
}

impl StaticUpcast<QObject> for Gui {
    unsafe fn static_upcast(ptr: Ptr<Self>) -> Ptr<QObject> {
        ptr.widget.as_ptr().static_upcast()
    }
}

impl Gui {
    fn new() -> Rc<Gui> {
        unsafe {
            let widget = QWidget::new_0a();
            let layout = QHBoxLayout::new_1a(&widget);

            let button = QPushButton::from_q_string(&qs("Start"));
            button.set_enabled(true);
            layout.add_widget(&button);

            let canvas = QOpenGLWidget::new_0a();
            canvas.set_minimum_size_1a(&QSize::new_2a(500, 400));
            layout.add_widget(&canvas);

            widget.show();

            let this = Rc::new(Self {
                widget,
                button,
                canvas,
            });
            this.init();
            this
        }
    }

    unsafe fn init(self: &Rc<Self>) {
        self.button
            .clicked()
            .connect(&self.slot_on_button_clicked());
    }

    #[slot(SlotNoArgs)]
    unsafe fn on_button_clicked(self: &Rc<Self>) {
        let text = qs("Hello!");
        QMessageBox::information_q_widget2_q_string(
            &self.widget,
            &qs("Example"),
            &qs("Text: \"%1\". Congratulations!").arg_q_string(&text),
        );
    }
}

fn main() {
    // This must remain the first call. mw_engine::application::Application takes care of this
    // for applications built upon the engine. The editor is a Qt application, however, so we have
    // to initialize the engine manually.
    Engine::initialize();

    QApplication::init(|_| unsafe {
        let _form = Gui::new();
        QApplication::exec()
    })
}
