use log::info;
use mw_engine::core::application::Application;
use mw_engine::core::event::Event;
use mw_engine::core::layer::Layer;
use mw_engine::core::scene::Scene;
use mw_engine::core::Engine;
use mw_engine::reexport::gl;
use mw_engine::renderer::buffer::VertexArray;
use mw_engine::renderer::shader::{ShaderProgram, ShaderType};
use mw_engine::renderer::{Buffers, Shaders};

fn main() {
    info!("Starting Many Worlds");
    let mut app = Application::new();
    let mut scene = Scene::new();
    scene
        .layer_stack()
        .push_layer(Box::new(OpenGLTestLayer::new()));
    app.set_scene(scene);
    app.run();
}

pub struct OpenGLTestLayer {
    // vertex_buffer: Box<dyn VertexBuffer>,
    // index_buffer: Box<dyn IndexBuffer>,
    shader_program: Box<dyn ShaderProgram>,
    vao: Box<dyn VertexArray>,
}

impl OpenGLTestLayer {
    pub fn new() -> Self {
        let vertex_shader = Shaders::new_shader(
            ShaderType::Vertex,
            std::fs::read_to_string("./mw-engine/shaders/opengl/test.vert")
                .expect("Couldn't load vertex shader source from file system."),
        );

        let fragment_shader = Shaders::new_shader(
            ShaderType::Fragment,
            std::fs::read_to_string("./mw-engine/shaders/opengl/test.frag")
                .expect("Couldn't load fragment shader source from file system."),
        );

        let tesselation_control_shader = Shaders::new_shader(
            ShaderType::TesselationControl,
            std::fs::read_to_string("./mw-engine/shaders/opengl/test.tcs.glsl")
                .expect("Couldn't load tesselation control shader source from file system."),
        );

        let tesselation_eval_shader = Shaders::new_shader(
            ShaderType::TesselationEvaluation,
            std::fs::read_to_string("./mw-engine/shaders/opengl/test.tes.glsl")
                .expect("Couldn't load tesselation evaluation shader source from file system."),
        );

        let geometry_shader = Shaders::new_shader(
            ShaderType::Geometry,
            std::fs::read_to_string("./mw-engine/shaders/opengl/test.geom")
                .expect("Couldn't load geometry shader source from file system."),
        );

        let mut shader_program = Shaders::new_shader_program();
        shader_program.attach(vertex_shader);
        shader_program.attach(tesselation_control_shader);
        shader_program.attach(tesselation_eval_shader);
        shader_program.attach(geometry_shader);
        shader_program.attach(fragment_shader);
        shader_program.link();

        unsafe {
            // wireframe mode on / off
            gl::PolygonMode(gl::FRONT_AND_BACK, gl::LINE);
            gl::PointSize(5.0f32);
        }

        OpenGLTestLayer {
            shader_program,
            vao: Buffers::new_vertex_array(1),
        }
    }
}

fn mouse_coords_in_clip_space() -> (f32, f32) {
    // Values as defined in the application window hints.
    // No method to get viewport geometry yet.
    let (width, height) = (1000, 700);
    let (x, y) = Engine::input().cursor_position();
    (
        x as f32 / width as f32 - 0.5,
        y as f32 / height as f32 - 0.5,
    )
}

impl Layer for OpenGLTestLayer {
    fn name(&self) -> &str {
        "OpenGL Test Layer"
    }

    fn on_update(&mut self) {
        // Nothing to do
    }

    fn on_render(&mut self) {
        let time = Engine::platform().time();
        let color = [
            time.sin() as f32 * 0.5f32 + 0.5f32,
            time.cos() as f32 * 0.5f32 + 0.5f32,
            0.0f32,
            1.0f32,
        ];
        self.vao.bind();
        self.shader_program.bind();

        unsafe {
            gl::ClearBufferfv(gl::COLOR, 0, &color as *const _);
            // set input-variables of vertex shader:
            // let offset = [
            //     (time.sin() * 0.5) as f32,
            //     (time.cos() * 0.6) as f32,
            //     0.0f32,
            //     0.0f32,
            // ];
            let (x, y) = mouse_coords_in_clip_space();
            let offset = [x, -y, 0.0f32, 0.0f32];
            gl::VertexAttrib4fv(0, &offset as *const _);

            // draw tessellated
            gl::DrawArrays(gl::PATCHES, 0, 3)
        }
    }

    fn on_event(&mut self, _event: &Event) -> bool {
        // Nothing to do
        false
    }

    fn is_enabled(&self) -> bool {
        // Nothing to do
        true
    }

    fn enable(&mut self) {
        // Nothing to do
    }

    fn disable(&mut self) {
        // Nothing to do
    }
}
